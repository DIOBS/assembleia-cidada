import pandas as pd
import geopandas as gpd

# Nota Tecnica: 
# Nao obtive problemas para encontrar os 12 setores censitarios requeridos utilizando os dados do Fortaleza em Mapas, 
# Ao usar as camadas do IBGE, 2 setores nao foram encontrados. 

DEBUG = True

# aquisicao dos setores censitarios por tipo de situacao
gdf_1 = gpd.read_file('./raw data/fortaleza-setores-sit1.geojson')
gdf_2 = gpd.read_file('./raw data/fortaleza-setores-sit2.geojson')
gdf_9 = gpd.read_file('./raw data/fortaleza-setores-sit9.geojson')
gdf_fm = gpd.read_file('./raw data/setores-censitarios-fm.geojson')

# concatenacao dos geodataframes
gdf = pd.concat([gdf_1,gdf_2,gdf_9])

# codigos dos setores para filtragem
codigos=['230440005070836', 
         '230440005120411', 
         '230440005080267', 
         '230440075130088',
         '230440070100141',
         '230440065110057',
         '230440005120698',
         '230440070140394',
         '230440065110236',
         '230440070100040',
         '230440060060124',
         '230440005080354']

gdf_filtered = gdf_fm[gdf_fm['ibge_codig'].isin(codigos)]

# verificando o filtro
if DEBUG:
    print('Codigos nao encontrados:')
    for cod in codigos:
        if cod not in gdf_filtered['ibge_codig'].to_list():
            print(cod)
    print(gdf_filtered.info())

# selecionando as colunas necessarias
gdf_filtered = gdf_filtered[['id','ibge_codig','area_setor','geometry']]
if DEBUG: print(gdf_filtered)

for cod in gdf_filtered['ibge_codig'].to_list():
    gdf = gdf_filtered[gdf_filtered['ibge_codig'] == cod]
    gdf.to_file('./processed data/setor_censitario_'+cod+'.geojson', driver='GeoJSON')

# exportando o geodataframe com os setores encontrados
gdf_filtered.to_file('./processed data/setores-censitarios-filtro.geojson',driver='GeoJSON')