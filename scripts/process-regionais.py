import pandas as pd
import geopandas as gpd

# geojson contendo as regionais
gdf_regionais = gpd.read_file('./utils/SER.geojson')

# remocao de colunas desnecessarias
gdf_regionais = gdf_regionais[['id','regiao_adm','geometry']]

print(gdf_regionais)

gdf_regionais.to_file('./processed data/regionais.geojson', driver='GeoJSON')

# gerando um arquivo geojson para cada regional
for id in gdf_regionais['id'].to_list():
    gdf = gdf_regionais[gdf_regionais['id']==id]
    gdf.to_file('./processed data/SER'+str(id)+'.geojson', driver='GeoJSON')
