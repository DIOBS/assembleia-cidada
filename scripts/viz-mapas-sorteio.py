import geopandas as gpd
import matplotlib.pyplot as plt

# ids das escolas sorteadas
sorteadas=[498, 286, 198, 484, 103, 507, 82, 172, 269, 379, 417, 549]

# tabela de escolas
gdf_escolas = gpd.read_file('processed data/2022_Escolas_Processed.geojson')

# filtro para as escolas sorteadas
gdf_sorteadas = gdf_escolas[gdf_escolas['id'].isin(sorteadas)]

# regionais de fortaleza
gdf_regionais = gpd.read_file('utils/SER.geojson')

# gerando grafico base
fig, ax = plt.subplots(figsize=(10,10))

# plotando as camadas
gdf_regionais.boundary.plot(ax=ax, linewidth=0.3, color='black')
gdf_escolas.plot(ax=ax, cmap='tab20b', column='tipo', legend=True)
gdf_sorteadas.plot(ax=ax, color='red', legend=True)

# exibindo o gráfico
ax.set_title('Instituições Escolares Municipais em Fortaleza', fontsize=10)
plt.axis('off')
plt.show()

# exportando para geojson e CSV
gdf_sorteadas.to_file('./processed data/2022_Escolas_Sorteadas.geojson', driver='GeoJSON')
gdf_sorteadas.to_csv('./processed data/2022_Escolas_Sorteadas.csv', encoding='utf-8')